<?php
defined('ABSPATH') or die('No script kiddies please!');

/**
 * @package HostPapa_Shortcodes
 * @version 1.0.0
 */
/*
Plugin Name: HostPapa Shortcodes
Plugin URI: https://hostpapa.ca
Description: This plugin acts as a centralized place where different shortcodes can be placed so they can be globally included in an orthodox and organized fashion.
Author: Pavel Espinal
Version: 1.0.0
Author URI: https://hostpapa.ca
*/

/**
 * Localized URLs
 *
 * [localized_url dst="blog|kb|home"]
 *
 * @param array $atts
 */
function localized_url($atts)
{
    // Declaring the list of recognized atrributes and
    // their default values.
    $recognized_attrs = shortcode_atts([
        'dst' => ''
    ], $atts);

    // From 'something.com' we take appart the parts (someting and com)
    $url_parts  = parse_url(site_url());

    $tld  = end(explode('.', $url_parts['host'])); // local.srv.hostpapa.[blog]
    $host = preg_replace("@\.$tld$@", '', $url_parts['host']); // [local.srv.hostpapa].blog

    $destination = $recognized_attrs['dst'];

    // If no param is sent, the default is 'home'
    if (! in_array($destination, ['blog', 'home', 'kb'])) {
        return home_url();
    }

    // Safe defaults
    $scheme   = "{$url_parts['scheme']}://";
    $location = "{$scheme}{$host}.{$tld}";

    switch ($destination) {
        case 'blog':
            if ($tld !== 'blog') {
                $location = "{$scheme}{$host}.{$tld}/blog";
            } else {
                $location = "{$scheme}{$host}.blog";
            }

            break;

        case 'kb':
            if ($host !== 'hostpapasupport') {
                $location = "{$scheme}{$host}.{$tld}/kb";
            }

            break;

        case 'home':
        default:
            return home_url();
            break;
    }

    return $location;
}

add_shortcode('localized_url', 'localized_url');
