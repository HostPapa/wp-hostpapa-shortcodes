# HostPapa Shortcodes

This plugin acts as a centralized place where different shortcodes can be placed in an organized fashion.

## Prerequisites

WordPress += 4.x

## Installing

Add the following to `composer.json`:

```json
repositories
{
    "type": "vcs",
    "url": "https://bitbucket.org/HostPapa/wp-hostpapa-shortcodes"
},
require {
    "hostpapa/wp-hostpapa-shortcodes": "^1.0"
}
```

To install, run:

```
$ composer install
```

## Usage
#### Localized URLs

`[localized_url dst="blog|kb|home"]`

This shortag will take a parameter either `blog`, `kb`, or `home` and provide the proper base URL for these three locations, based on the current TLD.

As a **general** rule, using the shortag under mydomain.com:

- `blog` will provide mydomain.com/blog
- `kb` will provide mydomain.com/kb
- `home` will provide mydomain.com

Exceptions:
Using the shortag under mydomain.blog:

- `blog` will provide mydomain.blog, not mydomain.blog/blog

Using the shortag under hostpapasupport.com (this **specific** domain):

- `kb` will provide hostpapasupport.com (no 'kb' appended)

If no destination (dst) parameter is sent, the default is 'home'.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/HostPapa/wp-hostpapa-shortcodes/downloads/?tab=tags).

## Authors

* **Pavel Espinal** - *Initial work*
